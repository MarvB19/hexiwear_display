#include <zephyr.h>
#include <device.h>
#include <drivers/display.h>
#include <stdio.h>
#include <drivers/spi.h>


void main(){

	const struct device *display_dev;
	struct display_capabilities capabilities;

    display_dev = device_get_binding(DT_LABEL(DT_N_INST_0_solomon_ssd1306fb));
    
	if (display_dev == NULL) {
		// LOG_ERR("Device %s not found. Aborting sample.",
		// 	DT_LABEL(DT_INST(0, solomon_ssd1306fb)));
		// RETURN_FROM_MAIN(1);
		printf("SSD1306 not found\n");
	}
	else{
		printf("SSD1306 found\n");
	}

    display_get_capabilities(display_dev, &capabilities);
    printf("%i\n",capabilities.y_resolution);
    printf("%i\n",capabilities.x_resolution);
    printf("%i\n",capabilities.screen_info);
    printf("%i\n",capabilities.y_resolution);

    printf("%i display_blanking_off(display_dev);  \n", display_blanking_off(display_dev));
    
}